# Integrated data analysis uncovers new COVID-19 related genes and potential drug re-purposing candidates.

This repository contains Python scripts and data for "Integrated data analysis uncovers new COVID-19 related genes and potential drug re-purposing candidates." by Alexandros Xenos*, Noël Malod-Dognin*, Carme Zambrana and Nataša Pržulj. 

In this work, to better understand the COVID-19 molecular basis and design therapeutic strategies, we build upon the recently proposed concept of an integrated cell, iCell, fusing three omics, tissue-specific human molecular interaction networks. We apply this methodology to construct infected and control iCells using gene expression data from patient samples and three cell lines. We find large differences between patient-based and cell line-based iCells, suggesting that cell lines are ill-suited to study this disease. We compare patient-based infected and control iCells and uncover genes whose functioning and network wiring patterns are altered by the disease. We validate in the literature that 18 out of the top 20 of the most rewired genes are indeed COVID-19 related. Since only three of these genes are targets of approved drugs, we apply another data fusion step to predict drugs for re-purposing. Our most interesting prediction is Artenimol, an antimalarial agent targeting ZFP62, one of the genes that we newly identified as COVID-19 related. This drug is a derivative of Artemisinin drugs that are already under clinical investigation for their potential role in the treatment of COVID-19. Our results demonstrate further applicability of the iCell framework in integrative comparative studies of human diseases. 

*the authors equally contributed.

Corresponding author: Prof. N. Przulj, e-mail: natasha@bsc.es

To generate the iCells run the "Main_create_iCells.py"

This script takes as parameter an integer number between 0 and 7 which corresponds to a tissue. Namely, 

0:'A549_Control', 1:'A549_Infected', 2:'NHBE_Control', 3:'NHBE_Infected', 4:'CALU_Control', 5:'CALU_Infected', 6'Patient_Control', 7: 'Patient_Infected'.

To perform the data fusion step to predict drugs for re-purposing run the "Main_drug_repurposing.py" which takes as input the G matrix factor of the patients' data and fuses it with the known drug target interactions. 
